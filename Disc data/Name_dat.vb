﻿Public Class Name_dat
    Public new_name As String = ""
    Private Sub Name_dat_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If CheckBox1.Checked Then
            TextBox1.Text = ""
        End If
        CheckBox1.Checked = True
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If TextBox1.Text <> "" And TextBox1.Text.IndexOf("/") = -1 And TextBox1.Text.IndexOf("\") = -1 And TextBox1.Text.IndexOf("<") = -1 And TextBox1.Text.IndexOf(">") = -1 And TextBox1.Text.IndexOf("|") = -1 And TextBox1.Text.IndexOf(":") = -1 And TextBox1.Text.IndexOf("?") = -1 Then
            new_name = TextBox1.Text
            Me.DialogResult = DialogResult.OK
            Me.Close()
        Else
            MsgBox("Имя содержит недопустимые символы: < > / \ | : ?",, "Ошибка")
        End If
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub
End Class
