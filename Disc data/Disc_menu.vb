﻿Public Class Disc_menu
    Public data(0) As String
    Dim disk(0) As Integer
    Public aktivate As Boolean = False
    Private Sub Disc_menu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If ListBox1.SelectedIndex = 0 Then
            Button2.Enabled = False
            Button3.Enabled = False
            Button5.Enabled = False
        Else
            Button2.Enabled = True
            Button3.Enabled = True
            Button5.Enabled = True
        End If
        If Read_data.WindowState = 2 Then
            Left = Read_data.Width / 2 - 240
            Top = Read_data.Height / 2 - 145
        ElseIf Read_data.WindowState = 0 Then
            Left = MousePosition.X
            Top = MousePosition.Y - 310
        End If
    End Sub
    Private Sub Disc_menu_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        disk_list("main")
    End Sub
    Private Sub Disk_menu_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If aktivate = True Then
            Progress.Activate()
        End If
    End Sub
    Private Sub ListBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
        If ListBox1.SelectedIndex = 0 Then
            Button2.Enabled = False
            Button3.Enabled = False
            Button5.Enabled = False
        Else
            Button2.Enabled = True
            Button3.Enabled = True
            Button5.Enabled = True
        End If
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        disk_create()
    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        disk_name()
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        disk_remove()
    End Sub
    Public Sub disk_list(ByVal tb As String)
        'Переменная временная
        Dim time_1 As String
        'Счётчик
        Dim i As Integer = 0
        If tb = "main" Then
            ListBox1.Items.Clear()
            ListBox1.Items.Add("--")
            ListBox1.SelectedIndex = 0
        ElseIf tb = "save" Then
            Disc_open.ListBox1.Items.Clear()
            Disc_open.ListBox1.Items.Add("--")
            Disc_open.ListBox1.SelectedIndex = 0
        End If
        Progress.ProgressBar1.Value = 0
        Dim value As Integer = Math.Floor(data.Length / 80)
        Progress.Show()
        aktivate = True
        For Each time As String In data
            time_1 = time.Length
            If Mid(time, 1, 1) = "<" And Mid(time, time_1, 1) = ">" Then
                Dim length As Integer = disk.Length
                Array.Resize(disk, length + 1)
                disk(length) = i
                If tb = "main" Then
                    ListBox1.Items.Add(Mid(time, 2, time_1 - 2))
                ElseIf tb = "save" Then
                    Disc_open.ListBox1.Items.Add(Mid(time, 2, time_1 - 2))
                End If
            End If
            If Math.IEEERemainder(i, value) = 0 Then
                Progress.ProgressBar1.Value += 1
            End If
            i = i + 1
        Next
        Progress.ProgressBar1.Value = 80
        aktivate = False
        Progress.Close()
    End Sub
    Public Sub disk_create()
        Dim length_1 As Integer = data.Length
        Dim length_2 As Integer = disk.Length
        Dim name As String = "New disc"
        Dim result As DialogResult
        New_name.Label2.Text = "Имя по-умолчанию: " & name
        result = New_name.ShowDialog()
        If result = DialogResult.OK Then
            name = New_name.TextBox1.Text
            If name = "" Then
                name = "New disc"
            End If
        End If
        name = Replace(name, "<", "")
        name = Replace(name, ">", "")
        New_name.Label2.Text = ""
        Array.Resize(data, length_1 + 1)
        data(length_1) = "<" & name & ">"
        Array.Resize(disk, length_2 + 1)
        disk(length_2) = length_1
        disk_write()
        disk_list("main")
    End Sub
    Public Sub disk_name()
        Dim name As String = "New disc"
        Dim result As DialogResult
        Dim selectedIndex As Integer
        selectedIndex = ListBox1.SelectedIndex - 1
        result = New_name.ShowDialog()
        If result = DialogResult.OK Then
            name = New_name.TextBox1.Text
            If name = "" Then
                name = "New disc"
            End If
        End If
        name = Replace(name, "<", "")
        name = Replace(name, ">", "")
        data(disk(selectedIndex + 1)) = "<" & name & ">"
        disk_write()
        disk_list("main")
    End Sub
    Public Sub disk_remove()
        Dim selectedIndex As Integer = disk(ListBox1.SelectedIndex - 1)
        Dim eof As Boolean = False
        Dim i As Integer = 1
        Dim start As Integer
        While eof <> True
            Dim time As String = data(selectedIndex + i)
            If Mid(time, 1, 1) = "<" And Mid(time, time.Length, 1) = ">" Then
                start = selectedIndex + i
                eof = True
            Else
                data(selectedIndex + i) = ""
                i = i + 1
            End If
        End While
        eof = False
        While eof <> True
            If selectedIndex = start Then
                eof = True
            Else
                MsgBox(start)
                MsgBox(i)
                MsgBox(data.Length - selectedIndex)
                MsgBox(selectedIndex)
                For i = start To data.Length - selectedIndex
                    If selectedIndex = 0 Then
                    Else
                        data(selectedIndex + i - 1) = data(selectedIndex + i)
                        data(selectedIndex + i) = ""
                    End If
                Next
                start -= 1
            End If
        End While
        disk_write()
        disk_list("main")
    End Sub
    Public Function disk_read()
        Try
            data = IO.File.ReadAllLines(Application.StartupPath + "/data.dat")
            Return 1
        Catch ex As Exception
            Return 0
        End Try
    End Function
    Private Function disk_write()
        Try
            IO.File.WriteAllLines(Application.StartupPath + "/data.dat", data)
            Return 1
        Catch ex As Exception
            Return 0
        End Try
    End Function
End Class