﻿Public Class Main
    Public disk_data As New List(Of Disk_segment), load_data As Boolean = True
    Private Sub Main_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Try
            Dim filedata() As String = IO.File.ReadAllLines(Application.StartupPath & "/data.dat")
            For i As Integer = 0 To filedata.Length - 1
                If "<" & filedata(i).Trim("<", ">") & ">" = filedata(i) Then
                    disk_data.Add(New Disk_segment)
                    disk_data.Item(disk_data.Count - 1).way = filedata(i).Trim("<", ">")
                    If filedata(i + 1) <> "NotFiles>" Then
                        disk_data.Item(disk_data.Count - 1).name = Mid(filedata(i + 1), 2)
                        Dim simvol As String = "", i1 As Integer = i
                        While simvol <> ">"
                            simvol = filedata(i1)
                            i1 += 1
                        End While
                        For i2 As Integer = i + 2 To i1 - 2
                            Array.Resize(disk_data.Item(disk_data.Count - 1).data, disk_data.Item(disk_data.Count - 1).data.Length + 1)
                            disk_data.Item(disk_data.Count - 1).data(disk_data.Item(disk_data.Count - 1).data.Length - 1) = filedata(i2)
                        Next
                    End If
                End If
            Next
            Dim dc As New ArrayList
            For i As Integer = 0 To disk_data.Count - 1
                If disk_data(i).name = "" Then
                    For i1 As Integer = 0 To disk_data.Count - 1
                        If disk_data(i).way = disk_data(i1).way And i <> i1 Then
                            dc.Add(i)
                        End If
                    Next
                End If
            Next
            Dim i3 As Integer = 0
            For Each i As Integer In dc
                disk_data.RemoveAt(i - i3)
                i3 += 1
            Next
        Catch
            MsgBox("База данных повреждена!",, "Сообщение")
        End Try
        TreeView1.Enabled = False
        For i As Integer = 0 To disk_data.Count - 1
            Dim text_data() As String = disk_data(i).way.Split("/"), last_node As New TreeNode
            For i1 As Integer = 0 To text_data.Length - 1
                Dim time_dat As Boolean = True
                If i1 = 0 Then
                    For i2 As Integer = 0 To TreeView1.Nodes.Count - 1
                        If TreeView1.Nodes.Item(i2).Name = text_data(i1) Then
                            time_dat = False
                        End If
                    Next
                Else
                    Dim node_time As TreeNode = TreeView1.SelectedNode
                    TreeView1.SelectedNode = last_node
                    For i2 As Integer = 0 To TreeView1.SelectedNode.Nodes.Count - 1
                        If TreeView1.SelectedNode.Nodes.Item(i2).Name = text_data(i1) Then
                            time_dat = False
                        End If
                    Next
                    TreeView1.SelectedNode = node_time
                End If
                If i1 = 0 Then
                    If time_dat = True Then
                        TreeView1.Nodes.Add(text_data(i1), text_data(i1), 2)
                    End If
                    TreeView1.SelectedNode = TreeView1.Nodes.Find(text_data(i1), False)(0)
                Else
                    If time_dat = True Then
                        last_node = TreeView1.SelectedNode
                        TreeView1.SelectedNode.Nodes.Add(text_data(i1), text_data(i1), 0)
                    End If
                    If text_data.Length > 2 Then
                        TreeView1.SelectedNode = TreeView1.SelectedNode.Nodes.Find(text_data(i1), False)(0)
                    End If
                End If
            Next
        Next
        TreeView1.CollapseAll()
        TreeView1.Enabled = True
        load_data = False
    End Sub
    Public Function GetFiles(way As String)
        Dim files(0) As String
        Array.Resize(files, 0)
        For i As Integer = 0 To disk_data.Count - 1
            If disk_data(i).way = way And disk_data(i).name <> "" Then
                Array.Resize(files, files.Length + 1)
                files(files.Length - 1) = way & "/" & disk_data(i).name
            End If
        Next
        Return files
    End Function
    Public Function DirExits(dir As String)
        Dim exits As Boolean = False
        For i As Integer = 0 To disk_data.Count - 1
            If disk_data(i).way.IndexOf(dir) = 0 Then
                exits = True
                Exit For
            End If
        Next
        Return exits
    End Function
    Public Function FileExits(file As String)
        Dim exits As Boolean = False
        For i As Integer = 0 To disk_data.Count - 1
            If disk_data(i).way & "/" & disk_data(i).name = file Then
                exits = True
                Exit For
            End If
        Next
        Return exits
    End Function
    Public Sub WriteFile(file As String, data() As String)
        Dim data_file() As String = file.Split("/"), way As String = "", name As String = ""
        For i As Integer = 0 To data_file.Length - 2
            If i = data_file.Length - 2 Then
                way &= data_file(i)
            Else
                way &= data_file(i) & "/"
            End If
        Next
        name = data_file(data_file.Length - 1)
        If FileExits(file) Then
            For i As Integer = 0 To disk_data.Count - 1
                If disk_data(i).way = way And disk_data(i).name = name Then
                    Array.Resize(disk_data(i).data, data.Length)
                    For i1 As Integer = 0 To data.Length - 1
                        disk_data(i).data(i1) = data(i1)
                    Next
                    Exit For
                End If
            Next
        Else
            disk_data.Add(New Disk_segment)
            disk_data.Item(disk_data.Count - 1).way = way
            disk_data.Item(disk_data.Count - 1).name = name
            Array.Resize(disk_data(disk_data.Count - 1).data, data.Length)
            For i As Integer = 0 To data.Length - 1
                disk_data(disk_data.Count - 1).data(i) = data(i)
            Next
        End If
    End Sub
    Public Function CreateDir(dir As String)
        If DirExits(dir) = False Then
            disk_data.Add(New Disk_segment)
            disk_data.Item(disk_data.Count - 1).way = dir
            disk_data.Item(disk_data.Count - 1).name = ""
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub DeleteFile(file As String)
        If FileExits(file) Then
            For i As Integer = 0 To disk_data.Count - 1
                If disk_data(i).way & "/" & disk_data(i).name = file Then
                    disk_data.RemoveAt(i)
                    Exit For
                End If
            Next
            Dim data_file() As String = file.Split("/"), way As String = ""
            For i As Integer = 0 To data_file.Length - 2
                If i = data_file.Length - 2 Then
                    way &= data_file(i)
                Else
                    way &= data_file(i) & "/"
                End If
            Next
            If DirExits(way) = False Then
                CreateDir(way)
            End If
        End If
    End Sub
    Public Sub DeleteDir(dir As String)
        If DirExits(dir) Then
            Dim dc As New ArrayList
            For i As Integer = 0 To disk_data.Count - 1
                If disk_data(i).way = dir Then
                    dc.Add(i)
                ElseIf disk_data(i).way.IndexOf(dir & "/") = 0 Then
                    dc.Add(i)
                End If
            Next
            Dim i1 As Integer = 0
            For Each i As Integer In dc
                disk_data.RemoveAt(i - i1)
                i1 += 1
            Next
        End If
    End Sub
    Public Sub RenameDir(dir As String, new_name As String)
        Dim data_file() As String = dir.Split("/"), way As String = ""
        For i As Integer = 0 To data_file.Length - 2
            If i = data_file.Length - 2 Then
                way &= data_file(i)
            Else
                way &= data_file(i) & "/"
            End If
        Next
        If DirExits(dir) Then
            For i As Integer = 0 To disk_data.Count - 1
                If disk_data(i).way = dir Then
                    disk_data(i).way = way & "/" & new_name
                End If
            Next
        End If
    End Sub
    Public Sub RenameFile(file As String, new_name As String)
        Dim data_file() As String = file.Split("/"), way As String = ""
        For i As Integer = 0 To data_file.Length - 2
            If i = data_file.Length - 2 Then
                way &= data_file(i)
            Else
                way &= data_file(i) & "/"
            End If
        Next
        If FileExits(file) Then
            For i As Integer = 0 To disk_data.Count - 1
                If disk_data(i).way & "/" & disk_data(i).name = file Then
                    disk_data(i).name = new_name
                End If
            Next
        End If
    End Sub
    Public Sub RenovateListView()
        ListView1.Items.Clear()
        Dim data_load() As String = GetFiles(TreeView1.SelectedNode.FullPath)
        For Each i As String In data_load
            Dim item As New ListViewItem, dat_time() As String = i.Split("/")
            item.Text = dat_time(dat_time.Length - 1)
            item.ImageIndex = 3
            ListView1.Items.Add(item)
        Next
    End Sub
    Public Function ReadFile(file As String)
        Dim data() As String = {}
        If FileExits(file) Then
            For i As Integer = 0 To disk_data.Count - 1
                If disk_data(i).way & "/" & disk_data(i).name = file Then
                    Array.Resize(data, disk_data(i).data.Length)
                    For i1 As Integer = 0 To data.Length - 1
                        data(i1) = disk_data(i).data(i1)
                    Next
                End If
            Next
        End If
        Return data
    End Function
    Public Sub RenowTreeView()
        Dim dc As New ArrayList
        For i As Integer = 0 To disk_data.Count - 1
            If disk_data(i).name = "" Then
                For i1 As Integer = 0 To disk_data.Count - 1
                    If disk_data(i).way = disk_data(i1).way And i <> i1 Then
                        dc.Add(i)
                    End If
                Next
            End If
        Next
        Dim i3 As Integer = 0
        For Each i As Integer In dc
            disk_data.RemoveAt(i - i3)
            i3 += 1
        Next
        If TreeView1.Nodes.Count = 0 Then
            ContextMenu1.Items.Item(1).Enabled = False
            ContextMenu1.Items.Item(3).Enabled = False
            ContextMenu1.Items.Item(5).Enabled = False
            ContextMenu2.Enabled = False
            ListView1.Items.Clear()
        Else
            ContextMenu1.Items.Item(1).Enabled = True
            ContextMenu1.Items.Item(3).Enabled = True
            ContextMenu1.Items.Item(5).Enabled = True
            ContextMenu2.Enabled = True
        End If
    End Sub
    Private Sub TreeView1_AfterExpand(sender As Object, e As TreeViewEventArgs) Handles TreeView1.AfterExpand
        If e.Node.Level > 0 Then
            e.Node.ImageIndex = 1
        End If
    End Sub
    Private Sub TreeView1_AfterCollapse(sender As Object, e As TreeViewEventArgs) Handles TreeView1.AfterCollapse
        If e.Node.Level > 0 Then
            e.Node.ImageIndex = 0
        End If
    End Sub
    Private Sub TreeView1_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles TreeView1.AfterSelect
        If load_data = False Then
            If DirExits(e.Node.FullPath) Then
                ListView1.Items.Clear()
                Dim data_load() As String = GetFiles(e.Node.FullPath)
                For Each i As String In data_load
                    Dim item As New ListViewItem, dat_time() As String = i.Split("/")
                    item.Text = dat_time(dat_time.Length - 1)
                    item.ImageIndex = 3
                    ListView1.Items.Add(item)
                Next
            End If
        End If
    End Sub
    Private Sub СоздатьПапкуToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles СоздатьПапкуToolStripMenuItem.Click
        Dim result As DialogResult = Name_dat.ShowDialog
        If result = DialogResult.Cancel Then
            Exit Sub
        End If
        If TreeView1.Nodes.Count = 0 Then
            If CreateDir(Name_dat.new_name) = True Then
                TreeView1.Nodes.Add(Name_dat.new_name, Name_dat.new_name, 2)
                RenowTreeView()
            End If
        Else
            If TreeView1.SelectedNode.Level = 0 Then
                If CreateDir(Name_dat.new_name) = True Then
                    TreeView1.Nodes.Add(Name_dat.new_name, Name_dat.new_name, 2)
                    RenowTreeView()
                End If
            Else
                Dim data_file() As String = TreeView1.SelectedNode.FullPath.Split("/"), way As String = ""
                For i As Integer = 0 To data_file.Length - 2
                    If i = data_file.Length - 2 Then
                        way &= data_file(i)
                    Else
                        way &= data_file(i) & "/"
                    End If
                Next
                If CreateDir(way & "/" & Name_dat.new_name) = True Then
                    RenowTreeView()
                    TreeView1.SelectedNode.Parent.Nodes.Add(Name_dat.new_name, Name_dat.new_name, 0)
                End If
            End If
        End If
    End Sub
    Private Sub ПереименоватьПапкуToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ПереименоватьПапкуToolStripMenuItem.Click
        Dim result As DialogResult = Name_dat.ShowDialog
        If result = DialogResult.OK Then
            RenameDir(TreeView1.SelectedNode.FullPath, Name_dat.new_name)
            TreeView1.SelectedNode.Tag = Name_dat.new_name
            TreeView1.SelectedNode.Text = Name_dat.new_name
            RenowTreeView()
        End If
    End Sub
    Private Sub УдалитьПапкуToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles УдалитьПапкуToolStripMenuItem.Click
        DeleteDir(TreeView1.SelectedNode.FullPath)
        TreeView1.SelectedNode.Remove()
        RenowTreeView()
        If load_data = False And TreeView1.Nodes.Count > 0 Then
            If DirExits(TreeView1.SelectedNode.FullPath) Then
                ListView1.Items.Clear()
                Dim data_load() As String = GetFiles(TreeView1.SelectedNode.FullPath)
                For Each i As String In data_load
                    Dim item As New ListViewItem, dat_time() As String = i.Split("/")
                    item.Text = dat_time(dat_time.Length - 1)
                    item.ImageIndex = 3
                    ListView1.Items.Add(item)
                Next
            End If
        End If
    End Sub
    Private Sub ОткрытьФайлToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ОткрытьФайлToolStripMenuItem.Click
        If ListView1.SelectedIndices.Count > 0 Then
            If FileExits(TreeView1.SelectedNode.FullPath & "/" & ListView1.SelectedItems(0).Text) Then
                Read_data.TextBox1.Lines = ReadFile(TreeView1.SelectedNode.FullPath & "/" & ListView1.SelectedItems(0).Text)
                Read_data.ShowDialog()
            End If
        End If
    End Sub
    Private Sub СоздатьФайлToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles СоздатьФайлToolStripMenuItem.Click
        Dim result As DialogResult = Name_dat.ShowDialog
        If result = DialogResult.OK Then
            If FileExits(TreeView1.SelectedNode.FullPath & "/" & Name_dat.new_name) = False Then
                Dim dat() As String = {}
                WriteFile(TreeView1.SelectedNode.FullPath & "/" & Name_dat.new_name, dat)
                RenovateListView()
                RenowTreeView()
            End If
        End If
    End Sub
    Private Sub УдалитьФайлToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles УдалитьФайлToolStripMenuItem.Click
        If ListView1.SelectedIndices.Count > 0 Then
            If FileExits(TreeView1.SelectedNode.FullPath & "/" & ListView1.SelectedItems(0).Text) Then
                DeleteFile(TreeView1.SelectedNode.FullPath & "/" & ListView1.SelectedItems(0).Text)
                RenovateListView()
                RenowTreeView()
            End If
        End If
    End Sub
    Private Sub ПереименоватьФайлToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ПереименоватьФайлToolStripMenuItem.Click
        Dim result As DialogResult = Name_dat.ShowDialog
        If ListView1.SelectedIndices.Count > 0 And result = DialogResult.OK Then
            If FileExits(TreeView1.SelectedNode.FullPath & "/" & ListView1.SelectedItems(0).Text) Then
                RenameFile(TreeView1.SelectedNode.FullPath & "/" & ListView1.SelectedItems(0).Text, Name_dat.new_name)
                RenovateListView()
            End If
        End If
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        load_data = True
        Try
            disk_data.Clear()
            Dim filedata() As String = IO.File.ReadAllLines(Application.StartupPath & "/data.dat")
            For i As Integer = 0 To filedata.Length - 1
                If "<" & filedata(i).Trim("<", ">") & ">" = filedata(i) Then
                    disk_data.Add(New Disk_segment)
                    disk_data.Item(disk_data.Count - 1).way = filedata(i).Trim("<", ">")
                    If filedata(i + 1) <> "NotFiles>" Then
                        disk_data.Item(disk_data.Count - 1).name = Mid(filedata(i + 1), 2)
                        Dim simvol As String = "", i1 As Integer = i
                        While simvol <> ">"
                            simvol = filedata(i1)
                            i1 += 1
                        End While
                        For i2 As Integer = i + 2 To i1 - 2
                            Array.Resize(disk_data.Item(disk_data.Count - 1).data, disk_data.Item(disk_data.Count - 1).data.Length + 1)
                            disk_data.Item(disk_data.Count - 1).data(disk_data.Item(disk_data.Count - 1).data.Length - 1) = filedata(i2)
                        Next
                    End If
                End If
            Next
            Dim dc As New ArrayList
            For i As Integer = 0 To disk_data.Count - 1
                If disk_data(i).name = "" Then
                    For i1 As Integer = 0 To disk_data.Count - 1
                        If disk_data(i).way = disk_data(i1).way And i <> i1 Then
                            dc.Add(i)
                        End If
                    Next
                End If
            Next
            Dim i3 As Integer = 0
            For Each i As Integer In dc
                disk_data.RemoveAt(i - i3)
                i3 += 1
            Next
        Catch
            MsgBox("База данных повреждена!",, "Сообщение")
        End Try
        TreeView1.Enabled = False
        ListView1.Items.Clear()
        TreeView1.Nodes.Clear()
        For i As Integer = 0 To disk_data.Count - 1
            Dim text_data() As String = disk_data(i).way.Split("/"), last_node As New TreeNode
            For i1 As Integer = 0 To text_data.Length - 1
                Dim time_dat As Boolean = True
                If i1 = 0 Then
                    For i2 As Integer = 0 To TreeView1.Nodes.Count - 1
                        If TreeView1.Nodes.Item(i2).Name = text_data(i1) Then
                            time_dat = False
                        End If
                    Next
                Else
                    Dim node_time As TreeNode = TreeView1.SelectedNode
                    TreeView1.SelectedNode = last_node
                    For i2 As Integer = 0 To TreeView1.SelectedNode.Nodes.Count - 1
                        If TreeView1.SelectedNode.Nodes.Item(i2).Name = text_data(i1) Then
                            time_dat = False
                        End If
                    Next
                    TreeView1.SelectedNode = node_time
                End If
                If i1 = 0 Then
                    If time_dat = True Then
                        TreeView1.Nodes.Add(text_data(i1), text_data(i1), 2)
                    End If
                    TreeView1.SelectedNode = TreeView1.Nodes.Find(text_data(i1), False)(0)
                Else
                    If time_dat = True Then
                        last_node = TreeView1.SelectedNode
                        TreeView1.SelectedNode.Nodes.Add(text_data(i1), text_data(i1), 0)
                    End If
                    If text_data.Length > 2 Then
                        TreeView1.SelectedNode = TreeView1.SelectedNode.Nodes.Find(text_data(i1), False)(0)
                    End If
                End If
            Next
        Next
        TreeView1.CollapseAll()
        TreeView1.Enabled = True
        load_data = False
    End Sub
    Private Sub СоздатьПодпапкуToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles СоздатьПодпапкуToolStripMenuItem.Click
        Dim result As DialogResult = Name_dat.ShowDialog
        If result = DialogResult.OK Then
            If CreateDir(TreeView1.SelectedNode.FullPath & "/" & Name_dat.new_name) = True Then
                TreeView1.SelectedNode.Nodes.Add(Name_dat.new_name, Name_dat.new_name, 0)
                RenowTreeView()
            End If
        End If
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim filedata(-1) As String
        For i As Integer = 0 To disk_data.Count - 1
            Array.Resize(filedata, filedata.Length + 2)
            filedata(filedata.Length - 2) = "<" & disk_data.Item(i).way & ">"
            If disk_data.Item(i).name = "" Then
                filedata(filedata.Length - 1) = "NotFiles>"
            Else
                filedata(filedata.Length - 1) = "<" & disk_data.Item(i).name
                For i1 As Integer = 0 To disk_data.Item(i).data.Length - 1
                    Array.Resize(filedata, filedata.Length + 1)
                    filedata(filedata.Length - 1) = disk_data.Item(i).data(i1)
                Next
                Array.Resize(filedata, filedata.Length + 1)
                filedata(filedata.Length - 1) = ">"
            End If
        Next
        IO.File.WriteAllLines(Application.StartupPath & "/data.dat", filedata)
    End Sub
    Private Sub ListView1_DoubleClick(sender As Object, e As EventArgs) Handles ListView1.DoubleClick
        If ListView1.SelectedIndices.Count > 0 Then
            If FileExits(TreeView1.SelectedNode.FullPath & "/" & ListView1.SelectedItems(0).Text) Then
                Read_data.TextBox1.Lines = ReadFile(TreeView1.SelectedNode.FullPath & "/" & ListView1.SelectedItems(0).Text)
                Read_data.ShowDialog()
            End If
        End If
    End Sub
End Class
Public Class Disk_segment
    Public way, name As String, data() As String
    Public Sub New()
        way = ""
        name = ""
        Array.Resize(data, 0)
    End Sub
End Class
