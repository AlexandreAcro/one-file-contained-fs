﻿Public Class Disc_open
    Private Sub Disc_open_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If ListBox1.SelectedIndex = 0 Then
            Button1.Enabled = False
            Button2.Enabled = False
        Else
            Button1.Enabled = True
            Button2.Enabled = True
        End If
        If Read_data.WindowState = 2 Then
            Left = Read_data.Width / 2 - 240
            Top = Read_data.Height / 2 - 145
        ElseIf Read_data.WindowState = 0 Then
            Left = MousePosition.X - 480
            Top = MousePosition.Y - 310
        End If
    End Sub
    Private Sub Disc_open_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Disc_menu.disk_list("save")
    End Sub
    Private Sub Disk_open_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If Disc_menu.aktivate = True Then
            Progress.Activate()
        End If
    End Sub
    Private Sub ListBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
        If ListBox1.SelectedIndex = 0 Then
            Button1.Enabled = False
            Button2.Enabled = False
        Else
            Button1.Enabled = True
            Button2.Enabled = True
        End If
    End Sub
End Class