﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.ContextMenu1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.СоздатьПапкуToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.СоздатьПодпапкуToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ПереименоватьПапкуToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.УдалитьПапкуToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IL = New System.Windows.Forms.ImageList(Me.components)
        Me.Button2 = New System.Windows.Forms.Button()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.ContextMenu2 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ОткрытьФайлToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ПереименоватьФайлToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.СоздатьФайлToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.УдалитьФайлToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ContextMenu1.SuspendLayout()
        Me.ContextMenu2.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TreeView1
        '
        Me.TreeView1.ContextMenuStrip = Me.ContextMenu1
        Me.TreeView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TreeView1.ImageIndex = 0
        Me.TreeView1.ImageList = Me.IL
        Me.TreeView1.Indent = 20
        Me.TreeView1.ItemHeight = 20
        Me.TreeView1.Location = New System.Drawing.Point(0, 0)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.PathSeparator = "/"
        Me.TreeView1.SelectedImageIndex = 4
        Me.TreeView1.Size = New System.Drawing.Size(213, 277)
        Me.TreeView1.TabIndex = 6
        '
        'ContextMenu1
        '
        Me.ContextMenu1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.СоздатьПапкуToolStripMenuItem, Me.СоздатьПодпапкуToolStripMenuItem, Me.ToolStripSeparator2, Me.ПереименоватьПапкуToolStripMenuItem, Me.ToolStripSeparator1, Me.УдалитьПапкуToolStripMenuItem})
        Me.ContextMenu1.Name = "ContextMenu1"
        Me.ContextMenu1.ShowImageMargin = False
        Me.ContextMenu1.Size = New System.Drawing.Size(172, 104)
        '
        'СоздатьПапкуToolStripMenuItem
        '
        Me.СоздатьПапкуToolStripMenuItem.Name = "СоздатьПапкуToolStripMenuItem"
        Me.СоздатьПапкуToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.СоздатьПапкуToolStripMenuItem.Text = "Создать папку"
        '
        'СоздатьПодпапкуToolStripMenuItem
        '
        Me.СоздатьПодпапкуToolStripMenuItem.Name = "СоздатьПодпапкуToolStripMenuItem"
        Me.СоздатьПодпапкуToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.СоздатьПодпапкуToolStripMenuItem.Text = "Создать подпапку"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(168, 6)
        '
        'ПереименоватьПапкуToolStripMenuItem
        '
        Me.ПереименоватьПапкуToolStripMenuItem.Name = "ПереименоватьПапкуToolStripMenuItem"
        Me.ПереименоватьПапкуToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.ПереименоватьПапкуToolStripMenuItem.Text = "Переименовать папку"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(168, 6)
        '
        'УдалитьПапкуToolStripMenuItem
        '
        Me.УдалитьПапкуToolStripMenuItem.Name = "УдалитьПапкуToolStripMenuItem"
        Me.УдалитьПапкуToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.УдалитьПапкуToolStripMenuItem.Text = "Удалить папку"
        '
        'IL
        '
        Me.IL.ImageStream = CType(resources.GetObject("IL.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.IL.TransparentColor = System.Drawing.Color.Transparent
        Me.IL.Images.SetKeyName(0, "ClosedDir.gif")
        Me.IL.Images.SetKeyName(1, "OpenedDir.gif")
        Me.IL.Images.SetKeyName(2, "RootDir.gif")
        Me.IL.Images.SetKeyName(3, "File.gif")
        Me.IL.Images.SetKeyName(4, "SelectedDir.gif")
        '
        'Button2
        '
        Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button2.Location = New System.Drawing.Point(337, 302)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(175, 25)
        Me.Button2.TabIndex = 9
        Me.Button2.Text = "Сохранить изменения"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'ListView1
        '
        Me.ListView1.ContextMenuStrip = Me.ContextMenu2
        Me.ListView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListView1.FullRowSelect = True
        Me.ListView1.Location = New System.Drawing.Point(0, 0)
        Me.ListView1.MultiSelect = False
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(282, 277)
        Me.ListView1.SmallImageList = Me.IL
        Me.ListView1.TabIndex = 10
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.List
        '
        'ContextMenu2
        '
        Me.ContextMenu2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ОткрытьФайлToolStripMenuItem, Me.ПереименоватьФайлToolStripMenuItem, Me.ToolStripSeparator3, Me.СоздатьФайлToolStripMenuItem, Me.ToolStripSeparator4, Me.УдалитьФайлToolStripMenuItem})
        Me.ContextMenu2.Name = "ContextMenu1"
        Me.ContextMenu2.ShowImageMargin = False
        Me.ContextMenu2.Size = New System.Drawing.Size(169, 104)
        '
        'ОткрытьФайлToolStripMenuItem
        '
        Me.ОткрытьФайлToolStripMenuItem.Name = "ОткрытьФайлToolStripMenuItem"
        Me.ОткрытьФайлToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.ОткрытьФайлToolStripMenuItem.Text = "Открыть файл"
        '
        'ПереименоватьФайлToolStripMenuItem
        '
        Me.ПереименоватьФайлToolStripMenuItem.Name = "ПереименоватьФайлToolStripMenuItem"
        Me.ПереименоватьФайлToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.ПереименоватьФайлToolStripMenuItem.Text = "Переименовать файл"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(165, 6)
        '
        'СоздатьФайлToolStripMenuItem
        '
        Me.СоздатьФайлToolStripMenuItem.Name = "СоздатьФайлToolStripMenuItem"
        Me.СоздатьФайлToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.СоздатьФайлToolStripMenuItem.Text = "Создать файл"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(165, 6)
        '
        'УдалитьФайлToolStripMenuItem
        '
        Me.УдалитьФайлToolStripMenuItem.Name = "УдалитьФайлToolStripMenuItem"
        Me.УдалитьФайлToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.УдалитьФайлToolStripMenuItem.Text = "Удалить файл"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer1.Location = New System.Drawing.Point(13, 12)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.TreeView1)
        Me.SplitContainer1.Panel1MinSize = 100
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.ListView1)
        Me.SplitContainer1.Panel2MinSize = 100
        Me.SplitContainer1.Size = New System.Drawing.Size(499, 277)
        Me.SplitContainer1.SplitterDistance = 213
        Me.SplitContainer1.TabIndex = 11
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button1.Location = New System.Drawing.Point(13, 302)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(83, 25)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = "Обновить"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ClientSize = New System.Drawing.Size(524, 339)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Button2)
        Me.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.MinimumSize = New System.Drawing.Size(382, 200)
        Me.Name = "Main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "База данных"
        Me.ContextMenu1.ResumeLayout(False)
        Me.ContextMenu2.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TreeView1 As TreeView
    Friend WithEvents Button2 As Button
    Friend WithEvents IL As ImageList
    Friend WithEvents ListView1 As ListView
    Friend WithEvents SplitContainer1 As SplitContainer
    Friend WithEvents ContextMenu1 As ContextMenuStrip
    Friend WithEvents СоздатьПапкуToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents СоздатьПодпапкуToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents УдалитьПапкуToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ContextMenu2 As ContextMenuStrip
    Friend WithEvents СоздатьФайлToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents УдалитьФайлToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents ПереименоватьПапкуToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ОткрытьФайлToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ПереименоватьФайлToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As ToolStripSeparator
    Friend WithEvents ToolStripSeparator4 As ToolStripSeparator
    Friend WithEvents Button1 As Button
End Class
