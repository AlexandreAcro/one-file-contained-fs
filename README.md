# Файловая система в одном файле (One File Contained FileSystem)
В данном репозитории представлены 2 программы: *Disc data* и *DD_OFC*. Вторая программа новее и не имеет некоторых ограничений первой программы.

## DD_OFC
Даннная программа реализует управление файловой системмой в одном файле. По факту, класс управления файловой системой находится в файле исходного кода с именем *OFC_FS.cs*. 
В данной файловой системе доступны все стандартные операции: создание папок и файлов переименование, копирование, перемещение и удаление. 
Эта файловая система имеет весь функционал, необходимый для превращения нескольких файлов в один. 

Файл с данными имеет имя *data.dat* и расположен в той же папке, что и программа.

Данную файловую систему предполагалось использовать в нескольких проектах, в том числе и в "*Universal Installer*". 
Но по иронии данная (более совершенная по сравнению с *Disc data*) файловая система полноценно нигде не была использована кроме программы-обозревателя (в этом репозитории).

Интерфейс программы приведён на изображении ниже:

![DD_OFC](DD_OFC.PNG)

Кстати, программа *DD_OFC.exe* реализует не все возможности *OFC_FS*, так как в программе нельзя создавать файлы в корне файловой системы т.е. без папки (т.к. к этому расположению нет доступа из интерфейса), но сама *OFC_FS* такое делать вполне позволяет.
Интересным фактом систем *Disc data* и *DD_OFC* является то, что в них фактически нет папок как структур: у файлов есть пути и части этих путей считаются папки. Для обозначения пустых папок введён специальный синтаксис.

## Disc data
Более старый вариант с похожим интерфейсом, но некоторыми отличиями. Здесь управление файловой системой разнесено по нескольким файлам, а не объединено в один класс. 
В данной файловой системе размещение файлов в корне недоступно всилу ограничений самой системы: путь к папке и имя файла задаются отдельно, при том, ни один, ни другой не могут быть пустыми.

Интерфейс программы приведён на изображении ниже:

![Disc data](Disc-data.PNG)

(корневые папки отмечены звёздочкой)

Части "движка" данной файловой системы были использованы в других проектах.

P.S. *Disc data.exe* имеет некоторые баги, однако, в других проектах, которые содержат части файловой системы *Disc data*, этих багов, кажется, нет. Эх, вот в *OFC_FS* багов нет (более тщательно разрабатывалаь и тестилась), но проекты с ней по итогу так и не были завершены... И - Ирония...