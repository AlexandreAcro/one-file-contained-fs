﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DD_OFC
{
    public partial class Copy_pos : Form
    {
        public Copy_pos()
        {
            InitializeComponent();
        }
        public byte result;
        private void button4_Click(object sender, EventArgs e)
        {
            result = 2;
            Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            result = 0;
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            result = 1;
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            result = 0;
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            result = 2;
            Close();
        }
    }
}
