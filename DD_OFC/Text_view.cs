﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DD_OFC
{
    public partial class Text_view : Form
    {
        public string current_way, current_name, arr_text = "";
        public Text_view()
        {
            InitializeComponent();
        }

        private void Text_view_Load(object sender, EventArgs e)
        {
            Reload_text();
            arr_text = textBox1.Text;
            label1.Text = "Сохранён";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Reload_text();
        }

        void Reload_text()
        {
           textBox1.Lines = Variables.FileSystem.OFC_FileData_read(current_way, current_name);
           textBox1.SelectionStart = 0; textBox1.SelectionLength = 0;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
           if (Text_compare(arr_text, textBox1.Text))
            {
                label1.Text = "Сохранён";
            }
            else
            {
                label1.Text = "Не сохранён";
            }
        }
        bool Text_compare(string text1, string text2)
        {
            if (text1.Length == text2.Length)
            {
                if (text1 == text2)
                {
                    return true;
                }
            }
            return false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Variables.FileSystem.OFC_FileData_write(current_way, current_name, textBox1.Lines);
            arr_text = textBox1.Text;
            label1.Text = "Сохранён";
        }
    }
}
