﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DD_OFC
{
    public partial class Main_form : Form
    {
        public Main_form()
        {
            InitializeComponent();
            if (System.IO.File.Exists(Application.StartupPath + "\\data.dat"))
                Variables.FileSystem.OFC_Fill_list_by_Array(System.IO.File.ReadAllLines(Application.StartupPath + "\\data.dat"));
            else
                System.IO.File.WriteAllText(Application.StartupPath + "\\data.dat", "");
        }
        private void Main_form_Shown(object sender, EventArgs e)
        {
            treeView1.BeginUpdate();
            foreach (TreeNode i in Construct_TreeView("")) {
                treeView1.Nodes.Add(i);
            }
            treeView1.EndUpdate();
        }

        Utilites.Copy_Entry[] copy_buffer = new Utilites.Copy_Entry[0]; string copy_way = ""; bool copy_mode_next = false;
        List<TreeNode> Construct_TreeView(string way)
        {
            string[] names = Variables.FileSystem.OFC_get_names(way, OFC_FS.OFC_Types.Folder); List<TreeNode> thc = new List<TreeNode>();
            if (names.Length == 0) return thc;
            Array.Sort(names);
            if (way == "")
            {
                for (ushort i = 0; i < names.Length; i++)
                {
                    thc.Add(Construct_TreeView_item(names[i], names[i]));
                }
            }
            else
            {
                for (ushort i = 0; i < names.Length; i++)
                {
                    thc.Add(Construct_TreeView_item(way+"\\"+names[i], names[i]));
                }
            }
            return thc;
        }
        TreeNode Construct_TreeView_item(string way, string name)
        {
            string[] names = Variables.FileSystem.OFC_get_names(way, OFC_FS.OFC_Types.Folder); TreeNode rtn = new TreeNode();
            rtn.Name = rtn.Text = name; rtn.ImageIndex = 0;
            if (names.Length==0) return rtn;
            Array.Sort(names);
            for (ushort i = 0; i < names.Length; i++)
            {
                rtn.Nodes.Add(Construct_TreeView_item(way+"\\"+ names[i], names[i]));
            }
            return rtn;
        }

        private void treeView1_AfterExpand(object sender, TreeViewEventArgs e)
        {
            e.Node.ImageIndex = 1;
            e.Node.SelectedImageIndex = 1;
        }

        private void treeView1_AfterCollapse(object sender, TreeViewEventArgs e)
        {
            e.Node.ImageIndex = 0;
            e.Node.SelectedImageIndex = 0;
        }
        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            Update_ListView(e.Node);
        }
        void Update_ListView(TreeNode node)
        {
            string[] names = new string[node.Nodes.Count];
            for (ushort i=0;i< names.Length;i++)
            {
                names[i] = node.Nodes[i].Text;
            }
            Array.Sort(names);
            listView1.BeginUpdate();
            listView1.Items.Clear();
            foreach (string i in names)
            {
                listView1.Items.Add(i,0);
            }
            names = Variables.FileSystem.OFC_get_names(node.FullPath, OFC_FS.OFC_Types.File);
            Array.Sort(names);
            foreach (string i in names)
            {
                listView1.Items.Add(i, 1);
            }
            listView1.EndUpdate();
        }
        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                if (listView1.SelectedItems[0].ImageIndex == 0)
                {
                    string text = listView1.SelectedItems[0].Text;
                    foreach (TreeNode i in treeView1.SelectedNode.Nodes)
                    {
                        if (i.Text == text)
                        {
                            treeView1.SelectedNode = i;
                        }
                    }
                }
                else
                {
                    Text_view form = new Text_view()
                    {
                        current_way = treeView1.SelectedNode.FullPath,
                        current_name = listView1.SelectedItems[0].Text
                    };
                    form.ShowDialog();
                    form.Dispose();
                }
            }
        }

        private void раскрытьВсеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            treeView1.ExpandAll();
        }

        private void закрытьВсеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            treeView1.CollapseAll();
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                if (listView1.SelectedItems[0].ImageIndex == 0)
                {
                    string text = listView1.SelectedItems[0].Text;
                    foreach (TreeNode i in treeView1.SelectedNode.Nodes)
                    {
                        if (i.Text == text)
                        {
                            treeView1.SelectedNode = i;
                        }
                    }
                }
                else
                {
                    Text_view form = new Text_view()
                    {
                        current_way = treeView1.SelectedNode.FullPath,
                        current_name = listView1.SelectedItems[0].Text
                    };
                    form.ShowDialog();
                    form.Dispose();
                }
            }
        }

        private void файлToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode == null)
            {
                MessageBox.Show("Вы не выбрали папку!", "Ошибка");
                return;
            }
            Enter_name form = new Enter_name()
            {
                current_way = treeView1.SelectedNode.FullPath,
                current_name = "New file",
                mode = 0
            };
            if (form.ShowDialog() == DialogResult.OK)
            {
                Variables.FileSystem.OFC_Make_entry(form.current_way, form.current_name, OFC_FS.OFC_Types.File);
                Update_ListView(treeView1.SelectedNode);
            }
            form.Dispose();
        }
        private void папкуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode == null)
            {
                MessageBox.Show("Вы не выбрали папку!", "Ошибка");
                return;
            }
            Enter_name form = new Enter_name()
            {
                current_way = treeView1.SelectedNode.FullPath,
                current_name = "New folder",
                mode = 1
            };
            if (form.ShowDialog() == DialogResult.OK)
            {
                Variables.FileSystem.OFC_Make_entry(form.current_way, form.current_name, OFC_FS.OFC_Types.Folder);
                Dictionary<string, bool> map = Utilites.Get_expand_map(treeView1.SelectedNode.Nodes);
                TreeNode n = treeView1.SelectedNode.Clone() as TreeNode;
                n.Nodes.Add(form.current_way, form.current_name, 0);
                treeView1.BeginUpdate();
                treeView1.SelectedNode.Nodes.Clear();
                foreach (TreeNode i in Sort_TreeView(n))
                    treeView1.SelectedNode.Nodes.Add(i);
                Utilites.Apply_expand_map(treeView1.SelectedNode.Nodes, map);
                treeView1.EndUpdate();
                Update_ListView(treeView1.SelectedNode);
            }
            form.Dispose();
        }
        List<TreeNode> Sort_TreeView(TreeNode node)
        {
            List<TreeNode> nodes = new List<TreeNode>();
            string[] names = new string[node.Nodes.Count];
            for (ushort i = 0; i< node.Nodes.Count;i++)
                names[i] = node.Nodes[i].Text;
            Array.Sort(names);
            foreach (string i in names)
                foreach (TreeNode n in node.Nodes)
                    if (n.Text == i) { nodes.Add(n); break; }
            return nodes;
        }

        private void переименоватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                if (listView1.SelectedItems[0].ImageIndex == 1)
                {
                    Enter_name form = new Enter_name()
                    {
                        current_way = treeView1.SelectedNode.FullPath,
                        current_name = listView1.SelectedItems[0].Text,
                        mode = 0
                    };
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        Variables.FileSystem.OFC_Rename_entry(form.current_way, listView1.SelectedItems[0].Text, form.current_name, OFC_FS.OFC_Types.File);
                        Update_ListView(treeView1.SelectedNode);
                    }
                    form.Dispose();
                }
                else
                {
                    Enter_name form = new Enter_name()
                    {
                        current_way = treeView1.SelectedNode.FullPath,
                        current_name = listView1.SelectedItems[0].Text,
                        mode = 1
                    };
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        string last_name = listView1.SelectedItems[0].Text;
                        Variables.FileSystem.OFC_Rename_entry(form.current_way, last_name, form.current_name, OFC_FS.OFC_Types.Folder);
                        treeView1.BeginUpdate();
                        foreach (TreeNode i in treeView1.SelectedNode.Nodes)
                            if (last_name == i.Text) { i.Text = form.current_name; break; }
                        Dictionary<string, bool> map = Utilites.Get_expand_map(treeView1.SelectedNode.Nodes);
                        TreeNode n = treeView1.SelectedNode.Clone() as TreeNode;
                        treeView1.SelectedNode.Nodes.Clear();
                        foreach (TreeNode i in Sort_TreeView(n))
                            treeView1.SelectedNode.Nodes.Add(i);
                        Utilites.Apply_expand_map(treeView1.SelectedNode.Nodes, map);
                        treeView1.EndUpdate();
                        Update_ListView(treeView1.SelectedNode);
                    }
                    form.Dispose();
                }
            }
            else { MessageBox.Show("Выберите элемент!","Ошибка");}
        }

        private void наОдномУровнеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode == null)
            {
                Enter_name form = new Enter_name()
                {
                    current_way = "",
                    current_name = "New folder",
                    mode = 1
                };
                if (form.ShowDialog() == DialogResult.OK)
                {
                    Variables.FileSystem.OFC_Make_entry("", form.current_name, OFC_FS.OFC_Types.Folder);
                    TreeNode n;
                    Dictionary<string, bool> map;
                    map = Utilites.Get_expand_map(treeView1.Nodes);
                    TreeNode[] arr = new TreeNode[treeView1.Nodes.Count];
                    for (int i = 0; i < treeView1.Nodes.Count; i++)
                        arr[i] = treeView1.Nodes[i].Clone() as TreeNode;
                    n = new TreeNode("", arr);
                    n.Nodes.Add(form.current_way, form.current_name, 0);
                    treeView1.BeginUpdate();
                    treeView1.Nodes.Clear();
                    foreach (TreeNode i in Sort_TreeView(n))
                        treeView1.Nodes.Add(i);
                    Utilites.Apply_expand_map(treeView1.Nodes, map);
                    treeView1.EndUpdate();
                }
                form.Dispose();
            }
            else
            {
                Enter_name form = new Enter_name()
                {
                    current_way = (treeView1.SelectedNode.Level == 0 ? "" : treeView1.SelectedNode.Parent.FullPath),
                    current_name = "New folder",
                    mode = 1
                };
                if (form.ShowDialog() == DialogResult.OK)
                {
                    Variables.FileSystem.OFC_Make_entry(form.current_way, form.current_name, OFC_FS.OFC_Types.Folder);
                    TreeNode n; string node_text = treeView1.SelectedNode.Text;
                    Dictionary<string, bool> map;
                    if (treeView1.SelectedNode.Level == 0)
                    {
                        map = Utilites.Get_expand_map(treeView1.Nodes);
                        TreeNode[] arr = new TreeNode[treeView1.Nodes.Count];
                        for (int i = 0; i < treeView1.Nodes.Count; i++)
                            arr[i] = treeView1.Nodes[i].Clone() as TreeNode;
                        n = new TreeNode("", arr);
                    }
                    else
                    {
                        map = Utilites.Get_expand_map(treeView1.SelectedNode.Parent.Nodes);
                        n = treeView1.SelectedNode.Parent.Clone() as TreeNode;
                    }
                    n.Nodes.Add(form.current_way, form.current_name, 0);
                    treeView1.BeginUpdate();
                    if (treeView1.SelectedNode.Level == 0)
                    {
                        treeView1.Nodes.Clear();
                        foreach (TreeNode i in Sort_TreeView(n))
                        {
                            treeView1.Nodes.Add(i);
                            if (i.Text == node_text) { treeView1.SelectedNode = i; }
                        }
                        Utilites.Apply_expand_map(treeView1.Nodes, map);
                    }
                    else
                    {
                        TreeNode n1 = treeView1.SelectedNode.Parent;
                        n1.Nodes.Clear();
                        foreach (TreeNode i in Sort_TreeView(n))
                        {
                            n1.Nodes.Add(i);
                            if (i.Text == node_text) { treeView1.SelectedNode = i; }
                        }
                        Utilites.Apply_expand_map(treeView1.SelectedNode.Parent.Nodes, map);
                    }
                    treeView1.EndUpdate();
                }
                form.Dispose();
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            treeView1.BeginUpdate();
            treeView1.Nodes.Clear();
            foreach (TreeNode i in Construct_TreeView(""))
            {
                treeView1.Nodes.Add(i);
            }
            treeView1.EndUpdate();
            treeView1.Update();
        }
        private void подпапкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode == null)
            {
                MessageBox.Show("Вы не выбрали папку!", "Ошибка");
                return;
            }
            Enter_name form = new Enter_name()
            {
                current_way = treeView1.SelectedNode.FullPath,
                current_name = "New folder",
                mode = 1
            };
            if (form.ShowDialog() == DialogResult.OK)
            {
                Variables.FileSystem.OFC_Make_entry(form.current_way, form.current_name, OFC_FS.OFC_Types.Folder);
                Dictionary<string, bool> map = Utilites.Get_expand_map(treeView1.SelectedNode.Nodes);
                TreeNode n = treeView1.SelectedNode.Clone() as TreeNode;
                n.Nodes.Add(form.current_way, form.current_name, 0);
                treeView1.BeginUpdate();
                treeView1.SelectedNode.Nodes.Clear();
                foreach (TreeNode i in Sort_TreeView(n))
                    treeView1.SelectedNode.Nodes.Add(i);
                Utilites.Apply_expand_map(treeView1.SelectedNode.Nodes, map);
                treeView1.EndUpdate();
                Update_ListView(treeView1.SelectedNode);
            }
            form.Dispose();
        }
       
        private void переименоватьПапкуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode == null)
            {
                MessageBox.Show("Вы не выбрали папку!", "Ошибка");
                return;
            }
            Enter_name form = new Enter_name()
            {
                current_way = (treeView1.SelectedNode.Level == 0 ? "" : treeView1.SelectedNode.Parent.FullPath),
                current_name = treeView1.SelectedNode.Text,
                mode = 1
            };
            if (form.ShowDialog() == DialogResult.OK)
            {
                TreeNode n; string node_text = treeView1.SelectedNode.Text;
                Variables.FileSystem.OFC_Rename_entry(form.current_way, node_text, form.current_name, OFC_FS.OFC_Types.Folder);
                Dictionary<string, bool> map;
                if (treeView1.SelectedNode.Level == 0)
                {
                    TreeNode[] arr = new TreeNode[treeView1.Nodes.Count];
                    for (int i = 0; i < treeView1.Nodes.Count; i++)
                    {
                        if (node_text == treeView1.Nodes[i].Text) { treeView1.Nodes[i].Text = form.current_name; }
                        arr[i] = treeView1.Nodes[i].Clone() as TreeNode;
                    }
                    n = new TreeNode("", arr);
                    map = Utilites.Get_expand_map(treeView1.Nodes);
                }
                else
                {
                    foreach (TreeNode i in treeView1.SelectedNode.Parent.Nodes)
                        if (node_text == i.Text) { i.Text = form.current_name; break; }
                    n = treeView1.SelectedNode.Parent.Clone() as TreeNode;
                    map = Utilites.Get_expand_map(treeView1.SelectedNode.Parent.Nodes);
                }
                treeView1.BeginUpdate();
                if (treeView1.SelectedNode.Level == 0)
                {
                    treeView1.Nodes.Clear();
                    foreach (TreeNode i in Sort_TreeView(n))
                    {
                        treeView1.Nodes.Add(i);
                        if (i.Text == form.current_name) { treeView1.SelectedNode = i; }
                    }
                    Utilites.Apply_expand_map(treeView1.Nodes, map);
                }
                else
                {
                    TreeNode n1 = treeView1.SelectedNode.Parent;
                    n1.Nodes.Clear();
                    foreach (TreeNode i in Sort_TreeView(n))
                    {
                        n1.Nodes.Add(i);
                        if (i.Text == form.current_name) { treeView1.SelectedNode = i; }
                    }
                    Utilites.Apply_expand_map(treeView1.SelectedNode.Parent.Nodes, map);
                }
                treeView1.EndUpdate();
            }
            form.Dispose();
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0) { MessageBox.Show("Выберите элементы!", "Ошибка"); return; }
            bool[] types_temp = new bool[2] {false,false};
            for (ushort i=0;i< listView1.SelectedItems.Count;i++)
            {
                if (listView1.SelectedItems[i].ImageIndex == 0)
                    types_temp[0] = true;
                else if (listView1.SelectedItems[i].ImageIndex == 1)
                    types_temp[1] = true;
                if (types_temp[0] && types_temp[1])
                    break;
            }
            if (types_temp[0] && types_temp[1])
            {
                if (MessageBox.Show("Вы действительно хотите удалить этот(ти) элемент(ы)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
            }
            else if (types_temp[0])
            { 
                if (MessageBox.Show("Вы действительно хотите удалить эту(ти) папку(и)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
            }
            else
            {
                if (MessageBox.Show("Вы действительно хотите удалить этот(ти) файл(ы)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
            }
            string[] tree_node_delete = new string[0];
            for (ushort i1 = 0; i1 < listView1.SelectedItems.Count; i1++)
            {
                if (listView1.SelectedItems[i1].ImageIndex == 1)
                {
                    Variables.FileSystem.OFC_Delete_entry(treeView1.SelectedNode.FullPath, listView1.SelectedItems[i1].Text, OFC_FS.OFC_Types.File, true);
                }
                else
                {
                    Variables.FileSystem.OFC_Delete_entry(treeView1.SelectedNode.FullPath, listView1.SelectedItems[i1].Text, OFC_FS.OFC_Types.Folder, true);
                    Array.Resize(ref tree_node_delete, tree_node_delete.Length + 1); tree_node_delete[tree_node_delete.Length - 1] = listView1.SelectedItems[i1].Text;
                }
            }
            if (types_temp[0])
            {
                treeView1.BeginUpdate();
                ushort l = (ushort)treeView1.SelectedNode.Nodes.Count;
                for (ushort i = 0; i < treeView1.SelectedNode.Nodes.Count; i++)
                {
                    for (ushort i1 = 0; i1 < tree_node_delete.Length; i1++)
                    {
                        if (treeView1.SelectedNode.Nodes[i].Text == tree_node_delete[i1])
                        {
                            treeView1.SelectedNode.Nodes.RemoveAt(i--);
                            break;
                        }
                    }
                }
                treeView1.EndUpdate();
            }
            Update_ListView(treeView1.SelectedNode);
            if (treeView1.SelectedNode.Nodes.Count == 0) treeView1.SelectedNode.Collapse();
        }
        private void удалитьПапкуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode == null) MessageBox.Show("Выберите элемент!", "Ошибка");
            if (MessageBox.Show("Вы действительно хотите удалить эту папку?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
            if (treeView1.SelectedNode.Level == 0)
            {
                Variables.FileSystem.OFC_Delete_entry("", treeView1.SelectedNode.Text, OFC_FS.OFC_Types.Folder, false);
                treeView1.SelectedNode.Remove();
            }
            else
            {
                Variables.FileSystem.OFC_Delete_entry(treeView1.SelectedNode.Parent.FullPath, treeView1.SelectedNode.Text, OFC_FS.OFC_Types.Folder, true);
                TreeNode parent = treeView1.SelectedNode.Parent;
                treeView1.SelectedNode.Remove();
                if (parent.Nodes.Count == 0) treeView1.SelectedNode.Collapse();
            }
            listView1.Items.Clear();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            System.IO.File.WriteAllLines(Application.StartupPath + "\\data.dat", Variables.FileSystem.OFC_export_to_arr());
        }
        
        private OFC_FS.OFC_ExistEntry_data Copy_poser(OFC_FS.OFC_ExistEntry_data poser_data)
        {
            Copy_pos form = new Copy_pos();
            if (poser_data.Poser_id == 1)
            {
                form.tableLayoutPanel1.Visible = false;
                form.label3.Text = "Ошибка: файл с таким именем уже существует в конечной папке!";
                form.label1.Text = "Путь исходного файла:";
                form.label2.Text = "Путь конечного файла:";
            }
            else
            {
                form.tableLayoutPanel1.Visible = true;
                form.label3.Text = "Ошибка: папка с таким именем уже существует в конечной папке!";
                form.label1.Text = "Путь исходной папки:";
                form.label2.Text = "Путь конечной папки:";
            }
            form.textBox1.Text = (poser_data.Way == "" ? "" : (poser_data.Way + "\\")) + poser_data.Name;
            form.textBox2.Text = (poser_data.Dest_entry_way == "" ? "" : (poser_data.Dest_entry_way + "\\")) + poser_data.Dest_entry_name;
            form.ShowDialog();
            if (form.result == 2)
            {
                poser_data.Action = OFC_FS.OFC_ExistEntry_action.SaveBoth;
            }
            else if (form.result == 1)
            {
                poser_data.Action = OFC_FS.OFC_ExistEntry_action.Replace;
            }
            else
            {
                poser_data.Action = OFC_FS.OFC_ExistEntry_action.NoCopy;
            }
            form.Dispose();
            return poser_data;
        }
        private void вставитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (copy_buffer.Length == 0) { MessageBox.Show("Вы не копировали элементы!", "Ошибка"); return; }
            if (treeView1.SelectedNode == null) { MessageBox.Show("Вы не выбрали место вставки!", "Ошибка"); return; }
            if (copy_way == treeView1.SelectedNode.FullPath) { MessageBox.Show("Исходный путь совпадает с конечным!", "Ошибка"); return; }
            bool[] types_temp = new bool[2] { false, false };
            for (ushort i = 0; i < copy_buffer.Length; i++)
            {
                if (copy_buffer[i].type == OFC_FS.OFC_Types.Folder)
                    types_temp[0] = true;
                else if (copy_buffer[i].type == OFC_FS.OFC_Types.File)
                    types_temp[1] = true;
                if (types_temp[0] && types_temp[1])
                    break;
            }
            if (copy_mode_next)
            {
                if (types_temp[0] && types_temp[1])
                {
                    if (types_temp[0]) { if (Utilites.No_Copy_Loop(copy_buffer,copy_way, treeView1.SelectedNode.FullPath)) { MessageBox.Show("Исходная папка содержит конечную!", "Ошибка"); return; } }
                    if (MessageBox.Show("Вы действительно хотите переместить этот(ти) элемент(ы)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
                    for (ushort i = 0; i < copy_buffer.Length; i++)
                    {
                        Variables.FileSystem.OFC_Copy_entry(copy_buffer[i].type, copy_way, copy_buffer[i].name, treeView1.SelectedNode.FullPath, copy_buffer[i].name, false, new OFC_FS.FileCopy_poser(Copy_poser));
                    }
                    treeView1.BeginUpdate();
                    if (copy_way == "")
                    {
                        for (ushort i = 0; i < treeView1.Nodes.Count; i++)
                        {
                            for (ushort i1 = 0; i1 < copy_buffer.Length; i1++)
                            {
                                if (treeView1.Nodes[i].Text == copy_buffer[i1].name)
                                {
                                    treeView1.Nodes.RemoveAt(i--);
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        TreeNode node1 = Utilites.Get_tree_node(treeView1.Nodes, copy_way);
                        for (ushort i = 0; i < node1.Nodes.Count; i++)
                        {
                            for (ushort i1 = 0; i1 < copy_buffer.Length; i1++)
                            {
                                if (node1.Nodes[i].Text == copy_buffer[i1].name)
                                {
                                    node1.Nodes.RemoveAt(i--);
                                    break;
                                }
                            }
                        }
                        if (node1.Nodes.Count == 0) node1.Collapse();
                    }
                    Dictionary<string, bool> map = Utilites.Get_expand_map(treeView1.SelectedNode.Nodes);
                    treeView1.SelectedNode.Nodes.Clear();
                    foreach (TreeNode i in Construct_TreeView(treeView1.SelectedNode.FullPath))
                    {
                        treeView1.SelectedNode.Nodes.Add(i);
                    }
                    Utilites.Apply_expand_map(treeView1.SelectedNode.Nodes, map);
                    treeView1.EndUpdate();
                    Update_ListView(treeView1.SelectedNode);
                }
                else if (types_temp[0])
                {
                    if (Utilites.No_Copy_Loop(copy_buffer, copy_way, treeView1.SelectedNode.FullPath)) { MessageBox.Show("Исходная папка содержит конечную!", "Ошибка"); return; }
                    if (MessageBox.Show("Вы действительно хотите переместить эту(ти) папку(и)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
                    for (ushort i = 0; i < copy_buffer.Length; i++)
                    {
                        Variables.FileSystem.OFC_Copy_entry(OFC_FS.OFC_Types.Folder, copy_way, copy_buffer[i].name, treeView1.SelectedNode.FullPath, copy_buffer[i].name, false, new OFC_FS.FileCopy_poser(Copy_poser));
                    }
                    treeView1.BeginUpdate();
                    if (copy_way == "")
                    {
                        for (ushort i = 0; i < treeView1.Nodes.Count; i++)
                        {
                            for (ushort i1 = 0; i1 < copy_buffer.Length; i1++)
                            {
                                if (treeView1.Nodes[i].Text == copy_buffer[i1].name)
                                {
                                    treeView1.Nodes.RemoveAt(i--);
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        TreeNode node1 = Utilites.Get_tree_node(treeView1.Nodes, copy_way);
                        for (ushort i = 0; i < node1.Nodes.Count; i++)
                        {
                            for (ushort i1 = 0; i1 < copy_buffer.Length; i1++)
                            {
                                if (node1.Nodes[i].Text == copy_buffer[i1].name)
                                {
                                    node1.Nodes.RemoveAt(i--);
                                    break;
                                }
                            }
                        }
                        if (node1.Nodes.Count == 0) node1.Collapse();
                    }
                    Dictionary<string, bool> map = Utilites.Get_expand_map(treeView1.SelectedNode.Nodes);
                    treeView1.SelectedNode.Nodes.Clear();
                    foreach (TreeNode i in Construct_TreeView(treeView1.SelectedNode.FullPath))
                    {
                        treeView1.SelectedNode.Nodes.Add(i);
                    }
                    Utilites.Apply_expand_map(treeView1.SelectedNode.Nodes, map);
                    treeView1.EndUpdate();
                    Update_ListView(treeView1.SelectedNode);
                }
                else
                {
                    if (MessageBox.Show("Вы действительно хотите переместить этот(ти) файл(ы)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
                    for (ushort i = 0; i < copy_buffer.Length; i++)
                    {
                        Variables.FileSystem.OFC_Copy_entry(OFC_FS.OFC_Types.File, copy_way, copy_buffer[i].name, treeView1.SelectedNode.FullPath, copy_buffer[i].name, false, new OFC_FS.FileCopy_poser(Copy_poser));
                    }
                    Update_ListView(treeView1.SelectedNode);
                }
                Array.Resize(ref copy_buffer, 0);
                copy_way = "";
                copy_mode_next = false;
                label1.Text = "Буфер обмена: свободен";
            }
            else
            {
                if (types_temp[0] && types_temp[1])
                {
                    if (types_temp[0]) { if (Utilites.No_Copy_Loop(copy_buffer, copy_way, treeView1.SelectedNode.FullPath)) { MessageBox.Show("Исходная папка содержит конечную!", "Ошибка"); return; } }
                    if (MessageBox.Show("Вы действительно хотите копировать этот(ти) элемент(ы)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
                    for (ushort i = 0; i < copy_buffer.Length; i++)
                    {
                        Variables.FileSystem.OFC_Copy_entry(copy_buffer[i].type, copy_way, copy_buffer[i].name, treeView1.SelectedNode.FullPath, copy_buffer[i].name, true, new OFC_FS.FileCopy_poser(Copy_poser));
                    }
                    treeView1.BeginUpdate();
                    Dictionary<string, bool> map = Utilites.Get_expand_map(treeView1.SelectedNode.Nodes);
                    treeView1.SelectedNode.Nodes.Clear();
                    foreach (TreeNode i in Construct_TreeView(treeView1.SelectedNode.FullPath))
                    {
                        treeView1.SelectedNode.Nodes.Add(i);
                    }
                    Utilites.Apply_expand_map(treeView1.SelectedNode.Nodes, map);
                    treeView1.EndUpdate();
                    Update_ListView(treeView1.SelectedNode);
                }
                else if (types_temp[0])
                {
                    if (Utilites.No_Copy_Loop(copy_buffer, copy_way, treeView1.SelectedNode.FullPath)) { MessageBox.Show("Исходная папка содержит конечную!", "Ошибка"); return; }
                    if (MessageBox.Show("Вы действительно хотите копировать эту(ти) папку(и)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
                    for (ushort i = 0; i < copy_buffer.Length; i++)
                    {
                        Variables.FileSystem.OFC_Copy_entry(OFC_FS.OFC_Types.Folder, copy_way, copy_buffer[i].name, treeView1.SelectedNode.FullPath, copy_buffer[i].name, true, new OFC_FS.FileCopy_poser(Copy_poser));
                    }
                    treeView1.BeginUpdate();
                    Dictionary<string, bool> map = Utilites.Get_expand_map(treeView1.SelectedNode.Nodes);
                    treeView1.SelectedNode.Nodes.Clear();
                    foreach (TreeNode i in Construct_TreeView(treeView1.SelectedNode.FullPath))
                    {
                        treeView1.SelectedNode.Nodes.Add(i);
                    }
                    Utilites.Apply_expand_map(treeView1.SelectedNode.Nodes, map);
                    treeView1.EndUpdate();
                    Update_ListView(treeView1.SelectedNode);
                }
                else
                {
                    if (MessageBox.Show("Вы действительно хотите копировать этот(ти) файл(ы)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
                    for (ushort i = 0; i < copy_buffer.Length; i++)
                    {
                        Variables.FileSystem.OFC_Copy_entry(OFC_FS.OFC_Types.File, copy_way, copy_buffer[i].name, treeView1.SelectedNode.FullPath, copy_buffer[i].name, true, new OFC_FS.FileCopy_poser(Copy_poser));
                    }
                    Update_ListView(treeView1.SelectedNode);
                }
            }
        }
        
        private void копироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0) { MessageBox.Show("Вы не выбрали элементы!", "Ошибка"); return; }
            Array.Resize(ref copy_buffer, listView1.SelectedItems.Count);
            for(ushort i=0;i< listView1.SelectedItems.Count;i++)
            {
                copy_buffer[i] = new Utilites.Copy_Entry(listView1.SelectedItems[i].Text, (listView1.SelectedItems[i].ImageIndex == 1 ? OFC_FS.OFC_Types.File : OFC_FS.OFC_Types.Folder));
            }
            copy_mode_next = false;
            copy_way = treeView1.SelectedNode.FullPath;
            label1.Text = "Буфер обмена: занят";
        }

        private void вырезатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0) { MessageBox.Show("Вы не выбрали элементы!", "Ошибка"); return; }
            Array.Resize(ref copy_buffer, listView1.SelectedItems.Count);
            for (ushort i = 0; i < listView1.SelectedItems.Count; i++)
            {
                copy_buffer[i] = new Utilites.Copy_Entry(listView1.SelectedItems[i].Text, (listView1.SelectedItems[i].ImageIndex == 1 ? OFC_FS.OFC_Types.File : OFC_FS.OFC_Types.Folder));
            }
            copy_mode_next = true;
            copy_way = treeView1.SelectedNode.FullPath;
            label1.Text = "Буфер обмена: занят";
        }
        private void очиститьToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Array.Resize(ref copy_buffer, 0);
            copy_way = "";
            copy_mode_next = false;
            label1.Text = "Буфер обмена: свободен";
        }

        private void вырезатьToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode == null) { MessageBox.Show("Вы не выбрали элементы!", "Ошибка"); return; }
            Array.Resize(ref copy_buffer, 1);
            copy_buffer[0] = new Utilites.Copy_Entry(treeView1.SelectedNode.Text, OFC_FS.OFC_Types.Folder);
            copy_mode_next = true;
            copy_way = (treeView1.SelectedNode.Level == 0 ? "" : treeView1.SelectedNode.Parent.FullPath);
            label1.Text = "Буфер обмена: занят";
        }

        private void копироватьToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode == null) { MessageBox.Show("Вы не выбрали элементы!", "Ошибка"); return; }
            Array.Resize(ref copy_buffer, 1);
            copy_buffer[0] = new Utilites.Copy_Entry(treeView1.SelectedNode.Text, OFC_FS.OFC_Types.Folder);
            copy_mode_next = false;
            copy_way = (treeView1.SelectedNode.Level == 0 ? "" : treeView1.SelectedNode.Parent.FullPath);
            label1.Text = "Буфер обмена: занят";
        }

        private void наОдномУровнеToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (copy_buffer.Length == 0) { MessageBox.Show("Вы не копировали элементы!", "Ошибка"); return; }
            if (treeView1.SelectedNode == null)
            {
                if (copy_way == "") { MessageBox.Show("Исходный путь совпадает с конечным!", "Ошибка"); return; }
                bool[] types_temp = new bool[2] { false, false };
                for (ushort i = 0; i < copy_buffer.Length; i++)
                {
                    if (copy_buffer[i].type == OFC_FS.OFC_Types.Folder)
                        types_temp[0] = true;
                    else if (copy_buffer[i].type == OFC_FS.OFC_Types.File)
                        types_temp[1] = true;
                    if (types_temp[0] && types_temp[1])
                        break;
                }
                if (types_temp[1]) { MessageBox.Show("Невозможно выполнить вставку файлов в этом месте!", "Ошибка"); return; }
                if (copy_mode_next)
                {
                    if (types_temp[0]) { if (Utilites.No_Copy_Loop(copy_buffer, copy_way, "")) { MessageBox.Show("Исходная папка содержит конечную!", "Ошибка"); return; } }
                    if (MessageBox.Show("Вы действительно хотите переместить этот(ти) элемент(ы)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
                    for (ushort i = 0; i < copy_buffer.Length; i++)
                    {
                        Variables.FileSystem.OFC_Copy_entry(copy_buffer[i].type, copy_way, copy_buffer[i].name, "", copy_buffer[i].name, false, new OFC_FS.FileCopy_poser(Copy_poser));
                    }
                    treeView1.BeginUpdate();
                    if (copy_way == "")
                    {
                        for (ushort i = 0; i < treeView1.Nodes.Count; i++)
                        {
                            for (ushort i1 = 0; i1 < copy_buffer.Length; i1++)
                            {
                                if (treeView1.Nodes[i].Text == copy_buffer[i1].name)
                                {
                                    treeView1.Nodes.RemoveAt(i--);
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        TreeNode node1 = Utilites.Get_tree_node(treeView1.Nodes, copy_way);
                        for (ushort i = 0; i < node1.Nodes.Count; i++)
                        {
                            for (ushort i1 = 0; i1 < copy_buffer.Length; i1++)
                            {
                                if (node1.Nodes[i].Text == copy_buffer[i1].name)
                                {
                                    node1.Nodes.RemoveAt(i--);
                                    break;
                                }
                            }
                        }
                        if (node1.Nodes.Count == 0) node1.Collapse();
                    }
                    Dictionary<string, bool> map = Utilites.Get_expand_map(treeView1.Nodes);
                    treeView1.Nodes.Clear();
                    foreach (TreeNode i in Construct_TreeView(""))
                    {
                        treeView1.Nodes.Add(i);
                    }
                    Utilites.Apply_expand_map(treeView1.Nodes, map);
                    treeView1.EndUpdate();
                    listView1.Items.Clear();
                    Array.Resize(ref copy_buffer, 0);
                    copy_way = "";
                    copy_mode_next = false;
                    label1.Text = "Буфер обмена: свободен";
                }
                else
                {
                    if (types_temp[0]) { if (Utilites.No_Copy_Loop(copy_buffer, copy_way, "")) { MessageBox.Show("Исходная папка содержит конечную!", "Ошибка"); return; } }
                    if (MessageBox.Show("Вы действительно хотите копировать этот(ти) элемент(ы)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
                    for (ushort i = 0; i < copy_buffer.Length; i++)
                    {
                        Variables.FileSystem.OFC_Copy_entry(copy_buffer[i].type, copy_way, copy_buffer[i].name, "", copy_buffer[i].name, true, new OFC_FS.FileCopy_poser(Copy_poser));
                    }
                    treeView1.BeginUpdate();
                    Dictionary<string, bool> map = Utilites.Get_expand_map(treeView1.Nodes);
                    treeView1.Nodes.Clear();
                    foreach (TreeNode i in Construct_TreeView(""))
                    {
                        treeView1.Nodes.Add(i);
                    }
                    Utilites.Apply_expand_map(treeView1.Nodes, map);
                    treeView1.EndUpdate();
                    listView1.Items.Clear();
                }
            }
            else
            {
                if (copy_way == (treeView1.SelectedNode.Level == 0 ? "" : treeView1.SelectedNode.Parent.FullPath)) { MessageBox.Show("Исходный путь совпадает с конечным!", "Ошибка"); return; }
                bool[] types_temp = new bool[2] { false, false };
                for (ushort i = 0; i < copy_buffer.Length; i++)
                {
                    if (copy_buffer[i].type == OFC_FS.OFC_Types.Folder)
                        types_temp[0] = true;
                    else if (copy_buffer[i].type == OFC_FS.OFC_Types.File)
                        types_temp[1] = true;
                    if (types_temp[0] && types_temp[1])
                        break;
                }
                if (types_temp[1] && treeView1.SelectedNode.Level == 0) { MessageBox.Show("Невозможно выполнить вставку файлов в этом месте!", "Ошибка"); return; }
                if (copy_mode_next)
                {
                    if (types_temp[0] && types_temp[1])
                    {
                        if (types_temp[0]) { if (Utilites.No_Copy_Loop(copy_buffer, copy_way, (treeView1.SelectedNode.Level == 0 ? "" : treeView1.SelectedNode.Parent.FullPath))) { MessageBox.Show("Исходная папка содержит конечную!", "Ошибка"); return; } }
                        if (MessageBox.Show("Вы действительно хотите переместить этот(ти) элемент(ы)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
                        for (ushort i = 0; i < copy_buffer.Length; i++)
                        {
                            Variables.FileSystem.OFC_Copy_entry(copy_buffer[i].type, copy_way, copy_buffer[i].name, (treeView1.SelectedNode.Level == 0 ? "" : treeView1.SelectedNode.Parent.FullPath), copy_buffer[i].name, false, new OFC_FS.FileCopy_poser(Copy_poser));
                        }
                        treeView1.BeginUpdate();
                        if (copy_way == "")
                        {
                            for (ushort i = 0; i < treeView1.Nodes.Count; i++)
                            {
                                for (ushort i1 = 0; i1 < copy_buffer.Length; i1++)
                                {
                                    if (treeView1.Nodes[i].Text == copy_buffer[i1].name)
                                    {
                                        treeView1.Nodes.RemoveAt(i--);
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            TreeNode node1 = Utilites.Get_tree_node(treeView1.Nodes, copy_way);
                            for (ushort i = 0; i < node1.Nodes.Count; i++)
                            {
                                for (ushort i1 = 0; i1 < copy_buffer.Length; i1++)
                                {
                                    if (node1.Nodes[i].Text == copy_buffer[i1].name)
                                    {
                                        node1.Nodes.RemoveAt(i--);
                                        break;
                                    }
                                }
                            }
                            if (node1.Nodes.Count == 0) node1.Collapse();
                        }
                        Dictionary<string, bool> map = Utilites.Get_expand_map((treeView1.SelectedNode.Level == 0 ? treeView1.Nodes : treeView1.SelectedNode.Parent.Nodes));
                        string node_text = treeView1.SelectedNode.Text;
                        if (treeView1.SelectedNode.Level == 0)
                        {
                            treeView1.Nodes.Clear();
                            foreach (TreeNode i in Construct_TreeView(""))
                            {
                                treeView1.Nodes.Add(i);
                                if (i.Text == node_text) { treeView1.SelectedNode = i; }
                            }
                        }
                        else
                        {
                            TreeNode n = treeView1.SelectedNode.Parent;
                            n.Nodes.Clear();
                            foreach (TreeNode i in Construct_TreeView(n.FullPath))
                            {
                                n.Nodes.Add(i);
                                if (i.Text == node_text) { treeView1.SelectedNode = i; }
                            }
                        }
                        Utilites.Apply_expand_map((treeView1.SelectedNode.Level == 0 ? treeView1.Nodes : treeView1.SelectedNode.Parent.Nodes), map);
                        treeView1.EndUpdate();
                        Update_ListView(treeView1.SelectedNode);
                    }
                    else if (types_temp[0])
                    {
                        if (Utilites.No_Copy_Loop(copy_buffer, copy_way, (treeView1.SelectedNode.Level == 0 ? "" : treeView1.SelectedNode.Parent.FullPath))) { MessageBox.Show("Исходная папка содержит конечную!", "Ошибка"); return; }
                        if (MessageBox.Show("Вы действительно хотите переместить эту(ти) папку(и)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
                        for (ushort i = 0; i < copy_buffer.Length; i++)
                        {
                            Variables.FileSystem.OFC_Copy_entry(copy_buffer[i].type, copy_way, copy_buffer[i].name, (treeView1.SelectedNode.Level == 0 ? "" : treeView1.SelectedNode.Parent.FullPath), copy_buffer[i].name, false, new OFC_FS.FileCopy_poser(Copy_poser));
                        }
                        treeView1.BeginUpdate();
                        if (copy_way == "")
                        {
                            for (ushort i = 0; i < treeView1.Nodes.Count; i++)
                            {
                                for (ushort i1 = 0; i1 < copy_buffer.Length; i1++)
                                {
                                    if (treeView1.Nodes[i].Text == copy_buffer[i1].name)
                                    {
                                        treeView1.Nodes.RemoveAt(i--);
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            TreeNode node1 = Utilites.Get_tree_node(treeView1.Nodes, copy_way);
                            for (ushort i = 0; i < node1.Nodes.Count; i++)
                            {
                                for (ushort i1 = 0; i1 < copy_buffer.Length; i1++)
                                {
                                    if (node1.Nodes[i].Text == copy_buffer[i1].name)
                                    {
                                        node1.Nodes.RemoveAt(i--);
                                        break;
                                    }
                                }
                            }
                            if (node1.Nodes.Count == 0) node1.Collapse();
                        }
                        Dictionary<string, bool> map = Utilites.Get_expand_map((treeView1.SelectedNode.Level == 0 ? treeView1.Nodes : treeView1.SelectedNode.Parent.Nodes));
                        string node_text = treeView1.SelectedNode.Text;
                        if (treeView1.SelectedNode.Level == 0)
                        {
                            treeView1.Nodes.Clear();
                            foreach (TreeNode i in Construct_TreeView(""))
                            {
                                treeView1.Nodes.Add(i);
                                if (i.Text == node_text) { treeView1.SelectedNode = i; }
                            }
                        }
                        else
                        {
                            TreeNode n = treeView1.SelectedNode.Parent;
                            n.Nodes.Clear();
                            foreach (TreeNode i in Construct_TreeView(n.FullPath))
                            {
                                n.Nodes.Add(i);
                                if (i.Text == node_text) { treeView1.SelectedNode = i; }
                            }
                        }
                        Utilites.Apply_expand_map((treeView1.SelectedNode.Level == 0 ? treeView1.Nodes : treeView1.SelectedNode.Parent.Nodes), map);
                        treeView1.EndUpdate();
                        Update_ListView(treeView1.SelectedNode);
                    }
                    else
                    {
                        if (MessageBox.Show("Вы действительно хотите переместить этот(ти) файл(ы)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
                        for (ushort i = 0; i < copy_buffer.Length; i++)
                        {
                            Variables.FileSystem.OFC_Copy_entry(copy_buffer[i].type, copy_way, copy_buffer[i].name, (treeView1.SelectedNode.Level == 0 ? "" : treeView1.SelectedNode.Parent.FullPath), copy_buffer[i].name, false, new OFC_FS.FileCopy_poser(Copy_poser));
                        }
                        Update_ListView(treeView1.SelectedNode);
                    }
                    Array.Resize(ref copy_buffer, 0);
                    copy_way = "";
                    copy_mode_next = false;
                    label1.Text = "Буфер обмена: свободен";
                }
                else
                {
                    if (types_temp[0] && types_temp[1])
                    {
                        if (types_temp[0]) { if (Utilites.No_Copy_Loop(copy_buffer, copy_way, (treeView1.SelectedNode.Level == 0 ? "" : treeView1.SelectedNode.Parent.FullPath))) { MessageBox.Show("Исходная папка содержит конечную!", "Ошибка"); return; } }
                        if (MessageBox.Show("Вы действительно хотите копировать этот(ти) элемент(ы)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
                        for (ushort i = 0; i < copy_buffer.Length; i++)
                        {
                            Variables.FileSystem.OFC_Copy_entry(copy_buffer[i].type, copy_way, copy_buffer[i].name, (treeView1.SelectedNode.Level == 0 ? "" : treeView1.SelectedNode.Parent.FullPath), copy_buffer[i].name, true, new OFC_FS.FileCopy_poser(Copy_poser));
                        }
                        treeView1.BeginUpdate();
                        Dictionary<string, bool> map = Utilites.Get_expand_map((treeView1.SelectedNode.Level == 0 ? treeView1.Nodes : treeView1.SelectedNode.Parent.Nodes));
                        string node_text = treeView1.SelectedNode.Text;
                        if (treeView1.SelectedNode.Level == 0)
                        {
                            treeView1.Nodes.Clear();
                            foreach (TreeNode i in Construct_TreeView(""))
                            {
                                treeView1.Nodes.Add(i);
                                if (i.Text == node_text) { treeView1.SelectedNode = i; }
                            }
                        }
                        else
                        {
                            TreeNode n = treeView1.SelectedNode.Parent;
                            n.Nodes.Clear();
                            foreach (TreeNode i in Construct_TreeView(n.FullPath))
                            {
                                n.Nodes.Add(i);
                                if (i.Text == node_text) { treeView1.SelectedNode = i; }
                            }
                        }
                        Utilites.Apply_expand_map((treeView1.SelectedNode.Level == 0 ? treeView1.Nodes : treeView1.SelectedNode.Parent.Nodes), map);
                        treeView1.EndUpdate();
                        Update_ListView(treeView1.SelectedNode);
                    }
                    else if (types_temp[0])
                    {
                        if (Utilites.No_Copy_Loop(copy_buffer, copy_way, (treeView1.SelectedNode.Level == 0 ? "" : treeView1.SelectedNode.Parent.FullPath))) { MessageBox.Show("Исходная папка содержит конечную!", "Ошибка"); return; }
                        if (MessageBox.Show("Вы действительно хотите копировать эту(ти) папку(и)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
                        for (ushort i = 0; i < copy_buffer.Length; i++)
                        {
                            Variables.FileSystem.OFC_Copy_entry(copy_buffer[i].type, copy_way, copy_buffer[i].name, (treeView1.SelectedNode.Level == 0 ? "" : treeView1.SelectedNode.Parent.FullPath), copy_buffer[i].name, true, new OFC_FS.FileCopy_poser(Copy_poser));
                        }
                        treeView1.BeginUpdate();
                        Dictionary<string, bool> map = Utilites.Get_expand_map((treeView1.SelectedNode.Level == 0 ? treeView1.Nodes : treeView1.SelectedNode.Parent.Nodes));
                        string node_text = treeView1.SelectedNode.Text;
                        if (treeView1.SelectedNode.Level == 0)
                        {
                            treeView1.Nodes.Clear();
                            foreach (TreeNode i in Construct_TreeView(""))
                            {
                                treeView1.Nodes.Add(i);
                                if (i.Text == node_text) { treeView1.SelectedNode = i; }
                            }
                        }
                        else
                        {
                            TreeNode n = treeView1.SelectedNode.Parent;
                            n.Nodes.Clear();
                            foreach (TreeNode i in Construct_TreeView(n.FullPath))
                            {
                                n.Nodes.Add(i);
                                if (i.Text == node_text) { treeView1.SelectedNode = i; }
                            }
                        }
                        Utilites.Apply_expand_map((treeView1.SelectedNode.Level == 0 ? treeView1.Nodes : treeView1.SelectedNode.Parent.Nodes), map);
                        treeView1.EndUpdate();
                        Update_ListView(treeView1.SelectedNode);
                    }
                    else
                    {
                        if (MessageBox.Show("Вы действительно хотите копировать этот(ти) файл(ы)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
                        for (ushort i = 0; i < copy_buffer.Length; i++)
                        {
                            Variables.FileSystem.OFC_Copy_entry(copy_buffer[i].type, copy_way, copy_buffer[i].name, (treeView1.SelectedNode.Level == 0 ? "" : treeView1.SelectedNode.Parent.FullPath), copy_buffer[i].name, true, new OFC_FS.FileCopy_poser(Copy_poser));
                        }
                        Update_ListView(treeView1.SelectedNode);
                    }
                }
            }
        }

        private void наПодуровнеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (copy_buffer.Length == 0) { MessageBox.Show("Вы не копировали элементы!", "Ошибка"); return; }
            if (treeView1.SelectedNode == null) { MessageBox.Show("Вы не выбрали место вставки!", "Ошибка"); return; }
            if (copy_way == treeView1.SelectedNode.FullPath) { MessageBox.Show("Исходный путь совпадает с конечным!", "Ошибка"); return; }
            bool[] types_temp = new bool[2] { false, false };
            for (ushort i = 0; i < copy_buffer.Length; i++)
            {
                if (copy_buffer[i].type == OFC_FS.OFC_Types.Folder)
                    types_temp[0] = true;
                else if (copy_buffer[i].type == OFC_FS.OFC_Types.File)
                    types_temp[1] = true;
                if (types_temp[0] && types_temp[1])
                    break;
            }
            if (copy_mode_next)
            {
                if (types_temp[0] && types_temp[1])
                {
                    if (types_temp[0]) { if (Utilites.No_Copy_Loop(copy_buffer, copy_way, treeView1.SelectedNode.FullPath)) { MessageBox.Show("Исходная папка содержит конечную!", "Ошибка"); return; } }
                    if (MessageBox.Show("Вы действительно хотите переместить этот(ти) элемент(ы)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
                    for (ushort i = 0; i < copy_buffer.Length; i++)
                    {
                        Variables.FileSystem.OFC_Copy_entry(copy_buffer[i].type, copy_way, copy_buffer[i].name, treeView1.SelectedNode.FullPath, copy_buffer[i].name, false, new OFC_FS.FileCopy_poser(Copy_poser));
                    }
                    treeView1.BeginUpdate();
                    if (copy_way == "")
                    {
                        for (ushort i = 0; i < treeView1.Nodes.Count; i++)
                        {
                            for (ushort i1 = 0; i1 < copy_buffer.Length; i1++)
                            {
                                if (treeView1.Nodes[i].Text == copy_buffer[i1].name)
                                {
                                    treeView1.Nodes.RemoveAt(i--);
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        TreeNode node1 = Utilites.Get_tree_node(treeView1.Nodes, copy_way);
                        for (ushort i = 0; i < node1.Nodes.Count; i++)
                        {
                            for (ushort i1 = 0; i1 < copy_buffer.Length; i1++)
                            {
                                if (node1.Nodes[i].Text == copy_buffer[i1].name)
                                {
                                    node1.Nodes.RemoveAt(i--);
                                    break;
                                }
                            }
                        }
                        if (node1.Nodes.Count == 0) node1.Collapse();
                    }
                    Dictionary<string, bool> map = Utilites.Get_expand_map(treeView1.SelectedNode.Nodes);
                    treeView1.SelectedNode.Nodes.Clear();
                    foreach (TreeNode i in Construct_TreeView(treeView1.SelectedNode.FullPath))
                    {
                        treeView1.SelectedNode.Nodes.Add(i);
                    }
                    Utilites.Apply_expand_map(treeView1.SelectedNode.Nodes, map);
                    treeView1.EndUpdate();
                    Update_ListView(treeView1.SelectedNode);
                }
                else if (types_temp[0])
                {
                    if (Utilites.No_Copy_Loop(copy_buffer, copy_way, treeView1.SelectedNode.FullPath)) { MessageBox.Show("Исходная папка содержит конечную!", "Ошибка"); return; }
                    if (MessageBox.Show("Вы действительно хотите переместить эту(ти) папку(и)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
                    for (ushort i = 0; i < copy_buffer.Length; i++)
                    {
                        Variables.FileSystem.OFC_Copy_entry(OFC_FS.OFC_Types.Folder, copy_way, copy_buffer[i].name, treeView1.SelectedNode.FullPath, copy_buffer[i].name, false, new OFC_FS.FileCopy_poser(Copy_poser));
                    }
                    treeView1.BeginUpdate();
                    if (copy_way == "")
                    {
                        for (ushort i = 0; i < treeView1.Nodes.Count; i++)
                        {
                            for (ushort i1 = 0; i1 < copy_buffer.Length; i1++)
                            {
                                if (treeView1.Nodes[i].Text == copy_buffer[i1].name)
                                {
                                    treeView1.Nodes.RemoveAt(i--);
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        TreeNode node1 = Utilites.Get_tree_node(treeView1.Nodes, copy_way);
                        for (ushort i = 0; i < node1.Nodes.Count; i++)
                        {
                            for (ushort i1 = 0; i1 < copy_buffer.Length; i1++)
                            {
                                if (node1.Nodes[i].Text == copy_buffer[i1].name)
                                {
                                    node1.Nodes.RemoveAt(i--);
                                    break;
                                }
                            }
                        }
                        if (node1.Nodes.Count == 0) node1.Collapse();
                    }
                    Dictionary<string, bool> map = Utilites.Get_expand_map(treeView1.SelectedNode.Nodes);
                    treeView1.SelectedNode.Nodes.Clear();
                    foreach (TreeNode i in Construct_TreeView(treeView1.SelectedNode.FullPath))
                    {
                        treeView1.SelectedNode.Nodes.Add(i);
                    }
                    Utilites.Apply_expand_map(treeView1.SelectedNode.Nodes, map);
                    treeView1.EndUpdate();
                    Update_ListView(treeView1.SelectedNode);
                }
                else
                {
                    if (MessageBox.Show("Вы действительно хотите переместить этот(ти) файл(ы)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
                    for (ushort i = 0; i < copy_buffer.Length; i++)
                    {
                        Variables.FileSystem.OFC_Copy_entry(OFC_FS.OFC_Types.File, copy_way, copy_buffer[i].name, treeView1.SelectedNode.FullPath, copy_buffer[i].name, false, new OFC_FS.FileCopy_poser(Copy_poser));
                    }
                    Update_ListView(treeView1.SelectedNode);
                }
                Array.Resize(ref copy_buffer, 0);
                copy_way = "";
                copy_mode_next = false;
                label1.Text = "Буфер обмена: свободен";
            }
            else
            {
                if (types_temp[0] && types_temp[1])
                {
                    if (types_temp[0]) { if (Utilites.No_Copy_Loop(copy_buffer, copy_way, treeView1.SelectedNode.FullPath)) { MessageBox.Show("Исходная папка содержит конечную!", "Ошибка"); return; } }
                    if (MessageBox.Show("Вы действительно хотите копировать этот(ти) элемент(ы)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
                    for (ushort i = 0; i < copy_buffer.Length; i++)
                    {
                        Variables.FileSystem.OFC_Copy_entry(copy_buffer[i].type, copy_way, copy_buffer[i].name, treeView1.SelectedNode.FullPath, copy_buffer[i].name, true, new OFC_FS.FileCopy_poser(Copy_poser));
                    }
                    treeView1.BeginUpdate();
                    Dictionary<string, bool> map = Utilites.Get_expand_map(treeView1.SelectedNode.Nodes);
                    treeView1.SelectedNode.Nodes.Clear();
                    foreach (TreeNode i in Construct_TreeView(treeView1.SelectedNode.FullPath))
                    {
                        treeView1.SelectedNode.Nodes.Add(i);
                    }
                    Utilites.Apply_expand_map(treeView1.SelectedNode.Nodes, map);
                    treeView1.EndUpdate();
                    Update_ListView(treeView1.SelectedNode);
                }
                else if (types_temp[0])
                {
                    if (Utilites.No_Copy_Loop(copy_buffer, copy_way, treeView1.SelectedNode.FullPath)) { MessageBox.Show("Исходная папка содержит конечную!", "Ошибка"); return; }
                    if (MessageBox.Show("Вы действительно хотите копировать эту(ти) папку(и)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
                    for (ushort i = 0; i < copy_buffer.Length; i++)
                    {
                        Variables.FileSystem.OFC_Copy_entry(OFC_FS.OFC_Types.Folder, copy_way, copy_buffer[i].name, treeView1.SelectedNode.FullPath, copy_buffer[i].name, true, new OFC_FS.FileCopy_poser(Copy_poser));
                    }
                    treeView1.BeginUpdate();
                    Dictionary<string, bool> map = Utilites.Get_expand_map(treeView1.SelectedNode.Nodes);
                    treeView1.SelectedNode.Nodes.Clear();
                    foreach (TreeNode i in Construct_TreeView(treeView1.SelectedNode.FullPath))
                    {
                        treeView1.SelectedNode.Nodes.Add(i);
                    }
                    Utilites.Apply_expand_map(treeView1.SelectedNode.Nodes, map);
                    treeView1.EndUpdate();
                    Update_ListView(treeView1.SelectedNode);
                }
                else
                {
                    if (MessageBox.Show("Вы действительно хотите копировать этот(ти) файл(ы)?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No) return;
                    for (ushort i = 0; i < copy_buffer.Length; i++)
                    {
                        Variables.FileSystem.OFC_Copy_entry(OFC_FS.OFC_Types.File, copy_way, copy_buffer[i].name, treeView1.SelectedNode.FullPath, copy_buffer[i].name, true, new OFC_FS.FileCopy_poser(Copy_poser));
                    }
                    Update_ListView(treeView1.SelectedNode);
                }
            }
        }
    }
    static class Utilites
    {
        public static bool No_Copy_Loop(Copy_Entry[] entries, string way, string select_way)
        {
            for (ushort i=0;i < entries.Length; i++)
            {
                if(entries[i].type == OFC_FS.OFC_Types.Folder)
                    if ((select_way+"\\").StartsWith((way == "" ? "" : way + "\\") + entries[i].name + "\\"))
                    {
                        return true;
                    }
            }
            return false;
        }
        public class Copy_Entry
        {
            public Copy_Entry(string e_name, OFC_FS.OFC_Types e_type)
            {
                name = e_name; type = e_type;
            }
            public string name; public OFC_FS.OFC_Types type;
        }
        public static TreeNode Get_tree_node(TreeNodeCollection c, string node_way)
        {
            string[] way;
            if (node_way.IndexOf('\\') == -1)
                way = new string[1] { node_way };
            else
                way = node_way.Split('\\');
            TreeNode answer=null;
            for (ushort i = 0; i < c.Count; i++)
            {
                if (way[0]==c[i].Text)
                {
                    if (way.Length > 1)
                    {
                        Get_tree_node_item(c[i], way, 1, answer);
                    }
                    else
                    {
                        answer = c[i];
                    }
                    break;
                }
            }
            return answer;
        }
        static void Get_tree_node_item(TreeNode n, string[] way, ushort index, TreeNode answer)
        {
            for (ushort i = 0; i < n.Nodes.Count; i++)
            {
                if (way[index] == n.Nodes[i].Text)
                {
                    if (way.Length > index+1)
                    {
                        Get_tree_node_item(n.Nodes[i], way, (ushort)(index + 1), answer);
                    }
                    else
                    {
                        answer = n.Nodes[i];
                    }
                    break;
                }
            }
        }
        public static void Apply_expand_map(TreeNodeCollection c, Dictionary<string, bool> map)
        {
            for (ushort i = 0; i < c.Count; i++)
            {
                Apply_expand_map_item(c[i], map);
            }
        }
        static void Apply_expand_map_item(TreeNode n, Dictionary<string, bool> map)
        {
            if (map.ContainsKey(n.FullPath + "\\" + n.Text))
                n.Expand();
            for (ushort i = 0; i < n.Nodes.Count; i++)
            {
                Apply_expand_map_item(n.Nodes[i], map);
            }
        }
        public static Dictionary<string, bool> Get_expand_map(TreeNodeCollection c)
        {
            Dictionary<string, bool> list = new Dictionary<string, bool>();
            for (ushort i = 0; i < c.Count; i++)
            {
                Get_expand_map_node(c[i], list);
            }
            return list;
        }
        static void Get_expand_map_node(TreeNode n, Dictionary<string, bool> list)
        {
            if (n.IsExpanded)
                list.Add(n.FullPath + "\\" + n.Text, true);
            for (ushort i = 0; i < n.Nodes.Count; i++)
            {
                Get_expand_map_node(n.Nodes[i], list);
            }
        }
    }
    static class Variables
    {
        public static OFC_FS FileSystem = new OFC_FS();
    }
}
